using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProgramaConsola.Codificadores;

namespace Tests
{
    [TestClass, TestCategory("Codificadores")]
    public class TecladoNumericoTest
    {
        private TecladoNumerico codificador = new TecladoNumerico();

        [DataTestMethod]
        [DataRow("hola mundo")]
        [DataRow("PROBANDO")]
        public void PuedeCodificar_DevuelveTrueParaCadenasSoportadas(string cadena) {
            Assert.IsTrue(codificador.PuedeCodificar(cadena));
        }

        [DataTestMethod]
        [DataRow("3728")]
        [DataRow("est4 c4d3n4 no deb3 funcion4r")]
        public void PuedeCodificar_DevuelveFalseParaCadenasNoSoportadas(string cadena) {
            Assert.IsFalse(codificador.PuedeCodificar(cadena));
        }

        [TestMethod]
        public void Codificar_FuncionaConLaCadena_hi()
        {
            var resultadoEsperado = "44 444";
            var resultado = codificador.Codificar("hi");

            Assert.AreEqual(resultadoEsperado, resultado);
        }

        [TestMethod]
        public void Codificar_FuncionaConLaCadena_yes()
        {
            var resultadoEsperado = "999337777";
            var resultado = codificador.Codificar("yes");

            Assert.AreEqual(resultadoEsperado, resultado);
        }

        [TestMethod]
        public void Codificar_FuncionaConLaCadena_foo_bar()
        {
            var resultadoEsperado = "333666 666022 2777";
            var resultado = codificador.Codificar("foo bar");

            Assert.AreEqual(resultadoEsperado, resultado);
        }

        [TestMethod]
        public void Codificar_FuncionaConLaCadena_hello_world()
        {
            var resultadoEsperado = "4433555 555666096667775553";
            var resultado = codificador.Codificar("hello world");

            Assert.AreEqual(resultadoEsperado, resultado);
        }

        [TestMethod]
        public void LaMismaPalabraEnMinuscula_Y_MayusculaDaElMismoResultado()
        {
            var a = codificador.Codificar("HOLA MUNDO");
            var b = codificador.Codificar("hola mundo");

            Assert.AreEqual(a, b);
        }
        
    }
}