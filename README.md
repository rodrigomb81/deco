# Que es esto?

Un programa que toma una cadena de caracteres [a-z ] y los codifica.

# Como se usa?
Hay que tener .NET Core SDK 5 y correr el siguiente comando en el dir "ProgramaConsola".

```
$ dotnet run -- "hola mundo"

4466655520688663666
```

Para ver la ayuda hay que escribir **--help** luego del doble guion.

# Que codificaciones soporta ?

Por ahora hay dos codificadores: "TecladoNumerico" y "Emoji".

### Teclado Numerico

Convierte una cadena de letras y espacios a la secuencia numerica que habria que presionar en un teclado numerico de telefono.

![](imagenes/codify1.gif)

### Emoji
Convierte palabras especificas al emoji correspondiente. Soporta cuatro palabras: anteojos, sonrisa, sol, y dino.

![](imagenes/codify2.gif)

# Liberias utilizadas

[Cocona: Micro framework for .NET console application](https://github.com/mayuki/Cocona)

Cocona se encarga de proveer el comando --help y de leer los argumentos que se pasan al correr la app.

# Que queda por hacer?

- [ ] Agregar mas codificadores ?

- [ ] Configurar el proyecto para compilarlo a un ejecutable autocontenido (que no requiera el sdk o el runtime)

- [ ] Crear un Codificador que pueda leer un mapeo de caracteres desde un archivo JSON

- [ ] Detectar usando reflexion las clases del namespace Codificadores que implementan ICodificador, en vez de tener
que instanciarlas a mano
