﻿using System.Collections.Generic;
using Cocona;
using ProgramaConsola.Codificadores;

namespace ProgramaConsola
{
    class Program
    {
        static void Main(string[] args)
        {
            CoconaApp.Run<Program>(args);
        }

        private Dictionary<string, ICodificador> codificadores = new Dictionary<string, ICodificador>();

        public Program()
        {
            // Instanciar los codificadores

            var keypad = new TecladoNumerico();
            codificadores.Add(keypad.Nombre, keypad);

            var emojificador = new Emojificador();
            codificadores.Add(emojificador.Nombre, emojificador);
        }

        public int Codificar(
            [Argument(Description = "La cadena que será codificada")]
            string cadena,

            [Argument(Description = "El codificar que se usará. Es TecladoNumerico por defecto")]
            string nombreCodificador = "TecladoNumerico"
        )
        {
            var codificador = codificadores[nombreCodificador];

            if (codificador.PuedeCodificar(cadena))
            {
                System.Console.WriteLine(codificador.Codificar(cadena));
                return 0;
            } else
            {
                System.Console.WriteLine($"El codificador '{codificador.Nombre}' no puede codificar esa cadena");
                return 1;
            }
        }
    }
}
