namespace ProgramaConsola.Codificadores
{
    interface ICodificador {
        string Nombre { get; }

        bool PuedeCodificar(string cadena);

        string Codificar(string cadena);
    }
}