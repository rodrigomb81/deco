using System.Text.RegularExpressions;

namespace ProgramaConsola.Codificadores
{
    // Codifica una lista de cadenas especificas a emojis
    public class Emojificador : ICodificador
    {
        public string Nombre => "Emoji";

        private Regex patronEsperado = new Regex(@"^(anteojos|sonrisa|dino|sol)$");

        public string Codificar(string cadena)
        {
            switch (cadena.ToLower())
            {
                case "anteojos":
                    return "😎";
                case "sonrisa":
                    return "😁";
                case "dino":
                    return "🦖";
                case "sol":
                    return "🌞";
                default:
                    throw new System.Exception("No conozco este emoji: " + cadena);
            }
        }

        public bool PuedeCodificar(string cadena)
        {
            return patronEsperado.IsMatch(cadena);
        }
    }
}